FROM node:10-alpine

WORKDIR /app

COPY package.json ./

RUN rm -fr node_modules

RUN npm install

EXPOSE 8025

COPY . .

CMD [ "npm", "run", "dev" ]