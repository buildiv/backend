import passport from 'passport';
import { ExtractJwt } from 'passport-jwt';
import LocalStrategy from 'passport-local';
import User from './components/users/userModel'

const JWTStrategy = require('passport-jwt').Strategy;

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: process.env.JWT_SECRET
}, async (payload, done) => {
  try {
    const user = await User.findById(payload.sub);

    if (!user) {
      return done(null, false);
    }

    done(null, user);
  } catch (error) {
    done(error, false);
  }
}));

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  async (email, password, done) => {
    try {
      const user = await User.findOne({ $or: [ { email }, { 'username': email } ] });

      if (!user) {
        return done(null, false)
      }

      const isMatch = await user.isValidPassword(password);

      if (!isMatch) {
        return done(null, false)
      }

      done(null, user);
    } catch (error) {
      done(error, null);
    }
  }));
