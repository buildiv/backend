import Joi from 'joi';

export function permit(...allowed) {
  const isAllowed = role => allowed.includes(role);

  return (req, res, next) => {
    if (req.user && isAllowed(req.user.role))
      next();
    else {
      res.status(403).json({
        message: "Forbidden"
      });
    }
  }
}

export function validateBody(schema) {
  return (req, res, next) => {
    const result = Joi.validate(req.body, schema);
    if (result.error) {
      return res.status(400).json(result.error);
    }

    if (!req.value) {
      req.value = {};
    }
    req.value['body'] = result.value;
    next();
  }
}

export const schemas = {
  authSchema: Joi.object().keys({
    name: Joi.string(),
    email: Joi.string().email().required(),
    username: Joi.string().required(),
    password: Joi.string().required()
  }),
  loginSchema: Joi.object().keys({
    username: Joi.string().required(),
    password: Joi.string().required()
  }),
  createMagicSchema: Joi.object().keys({
    email: Joi.string().email().required()
  }),
  magicSchema: Joi.object().keys({
    token: Joi.string().required()
  })
}
