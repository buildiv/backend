import Router from 'express';
import UserAPI from './userAPI';
import passport from 'passport';

import passportConf from '../../passport'; // eslint-disable-line

import { validateBody, schemas, permit }  from './userHelper';

const router = Router();

const passportJWT   = passport.authenticate('jwt',   { session: false });
const passportLocal = passport.authenticate('local', { session: false });

router.route('/')
  .get(UserAPI.default)

router.route('/secret')
  .get(passportJWT, permit('admin', 'moderator'), UserAPI.secret)
    
router.route('/signup')
  .post(validateBody(schemas.authSchema), UserAPI.signUp)
    
router.route('/login')
  .post(passportLocal, validateBody(schemas.loginSchema), UserAPI.signIn)


export default router;
