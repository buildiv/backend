import JWT from 'jsonwebtoken';
import User from './userModel';

function createToken(user) {
  return JWT.sign({
    iss: 'iss',
    sub: user.id,
    iat: new Date().getTime(),
    exp: new Date().setDate(new Date().getDate() + 1)
  }, process.env.JWT_SECRET)
}

export default {
  async default (req, res, next) {
    try {
      const users = await User.find({});

      res.json(users);
    } catch (error) {
      next(error);
    }
  },
  async secret(req, res, next) {
    try {
      const users = await User.find({});

      res.json(users);
    } catch (error) {
      next(error);
    }
  },
  async signUp(req, res, next) {
    try {
      const { name, username, email, password } = req.value.body;

      const foundUser = await User.findOne({ 'email': email });

      if (foundUser) {
        res.status(409).json({ error: 'Email is already in use'});
      } else {
        const newUser = new User({ name, username, email, password });

        await newUser.save();

        const token = createToken(newUser);
        const user = (({ name, email, username, role }) => ({ name, email, username, role }))(newUser);

        res.status(200).json({ token, user });
      }
    } catch (error) {
      next(error);
    }
  },
  async signIn(req, res, next) {
    try {
      const token = createToken(req.user);
      const user = (({ name, email, username, role }) => ({ name, email, username, role }))(req.user);

      res.status(200).json({ token , user });
    } catch (error) {
      next(error);
    }
  }
}
