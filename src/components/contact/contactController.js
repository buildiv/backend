import Router from 'express';
import ContactAPI from './contactAPI';
import passport from 'passport';

import { validateBody, schemas }  from './contactHelper';
import {  permit }  from '../users/userHelper';

const passportJWT = passport.authenticate('jwt',   { session: false });

const router = Router();

router.route('/')
  .post(validateBody(schemas.contactSchema), ContactAPI.default)

router.route('/')
  .get(passportJWT, permit('admin'), ContactAPI.all)

router.route('/')
  .delete(passportJWT, permit('admin'), ContactAPI.remove)

export default router;
