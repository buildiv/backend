import mognoose from 'mongoose';

const Nexmo = require('nexmo'); // eslint-disable-line

const nexmo = new Nexmo({ // eslint-disable-line
  apiKey: process.env.NEXMO,
  apiSecret: process.env.NEXMOSECRET
})

const contactSchema = new mognoose.Schema({
  email: {
    type: String,
    required: true
  },
  subject: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  },
  date: {
    type: String,
    default: Date.now
  }
});

contactSchema.pre('save', async function (next) {
  try {
    // let from = 'buildiv';
    // let to = process.env.PHONE;
    // let text = `${this.email} | ${this.message}`;

    // nexmo.message.sendSms(from, to, text);

    next();
  } catch (error) {
    next(error)
  }
})

const User = mognoose.model('contact', contactSchema);

export default User;
