import Contact from './contactModel';

export default {
  async default (req, res, next) {
    try {
      const { email, subject, message } = req.body;

      const contact = new Contact({ email, subject, message });

      await contact.save();

      res.status(200).json({
        message: "Mesajul a fost trimis"
      });
    } catch (error) {
      next(error);
    }
  },
  async all(req, res, next) {
    try {
      const contacts = await Contact.find({});

      res.json(contacts);
    } catch (error) {
      next(error);
    }
  },
  async remove(req, res, next) {
    try {
      const { _id } = req.query;
      await Contact.findOneAndRemove({ _id });

      res.status(200).json({

        message: "Mesajul a fost eliminat"
      });
    } catch (error) {
      next(error);
    }
  }
}
