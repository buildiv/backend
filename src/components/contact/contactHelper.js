import Joi from 'joi';

export function validateBody(schema) {
  return (req, res, next) => {
    const result = Joi.validate(req.body, schema);
    if (result.error) {
      return res.status(400).json(result.error);
    }

    if (!req.value) {
      req.value = {};
    }
    req.value['body'] = result.value;
    next();
  }
}

export const schemas = {
  contactSchema: Joi.object().keys({
    email: Joi.string().email().required(),
    subject: Joi.string().required(),
    message: Joi.string().required().max(70)
  })
}
