import Joi from 'joi';

export function validateBody(schema) {
  return (req, res, next) => {
    const result = Joi.validate(req.body, schema);
    if (result.error) {
      return res.status(400).json(result.error);
    }

    if (!req.value) {
      req.value = {};
    }
    req.value['body'] = result.value;
    next();
  }
}


export const schemas = {
  templateSchema: Joi.object().keys({
    href: Joi.string().required(),
    token: Joi.string().required(),
    html: Joi.string().required(),
    preview: Joi.string().required()
  })
}
