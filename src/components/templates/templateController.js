import Router from 'express';
import TemplateAPI from './templateAPI';
import passport from 'passport';

import { validateBody, schemas }  from './templateHelper';

const passportJWT = passport.authenticate('jwt', { session: false });

const router = Router();

router.route('/my')
  .get(passportJWT, TemplateAPI.my)

router.route('/')
  .get(TemplateAPI.all)
  
router.route('/')
  .post(validateBody(schemas.templateSchema), passportJWT, TemplateAPI.save)

router.route('/')
  .delete(passportJWT, TemplateAPI.remove)

router.route('/:href')
  .get(TemplateAPI.default)

router.route('/rename')
  .post(passportJWT, TemplateAPI.rename)

export default router;
