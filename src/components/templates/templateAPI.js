import Template from './templateModel';
import fs from 'fs';

const fsPromises = require('fs').promises;

export default {
  async default(req, res, next) {
    try {
      const { href } = req.params;
      const template = await Template.findOne({ href });

      res.json(template);
    } catch (error) {
      next(error);
    }
  },
  async all(req, res, next) {
    try {
      const templates = await Template.find({  });

      res.json(templates);
    } catch (error) {
      next(error);
    }
  },
  async my(req, res, next) {
    try {
      const user = req.user.username;
      const templates = await Template.find({ user });

      res.json(templates);
    } catch (error) {
      next(error);
    }
  },
  async save(req, res, next) {
    try {
      const { href, html } = req.body;
      const user = req.user.username;

      let template = await Template.findOne({ href, user });

      var base64Data = req.body.preview.replace(/^data:image\/png;base64,/, "");

      const dir = 'public/previews';
      
      if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
      }
      await fsPromises.writeFile(`${dir}/${href}.png`, base64Data, 'base64');

      if (template) {
        template.html = html;
      } else {
        template = new Template({ href, html, user });
      }
      
      await template.save();

      res.status(200).json({
        message: "Șablonul a fost a fost salvat"
      });
    } catch (error) {
      next(error);
    }
  },
  async rename(req, res, next) {
    try {
      const { href, title } = req.body;

      let template = await Template.findOne({ href });

      if (template) {
        template.title = title;
        
        await template.save();
        
        res.status(200).json({
          message: "Șablonul a fost a fost salvat"
        });
      }

    } catch (error) {
      next(error);
    }
  },
  async remove(req, res, next) {
    try {
      const { _id } = req.query;
      await Template.findOneAndRemove({ _id });

      res.status(200).json({
        message: "Șablonul a fost eliminat"
      });
    } catch (error) {
      next(error);
    }
  }
}
