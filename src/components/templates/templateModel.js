import mognoose from 'mongoose';

const TemplateSchema = new mognoose.Schema({
  href: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true,
    default: "Șablon nou"
  },
  user: {
    type: String,
    required: true
  },
  html: {
    type: String,
    required: true
  },
  created: {
    type: String,
    default: Date.now
  },
  edited: {
    type: String,
    default: Date.now
  }
});

TemplateSchema.pre('save', async function (next) {
  try {
    
    this.edited = await Date.now();

    next();
  } catch (error) {
    next(error)
  }
})

const Template = mognoose.model('template', TemplateSchema);

export default Template;
