import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import routes from './routes';
import cors from 'cors';
import fs from 'fs';

const app = express();

mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true
});


app.use(cors());
app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))

app.use(express.static(path.join(__dirname, '../public')));

app.use('/', routes);

app.use((req, res, next) => {
  const err = new Error('404 - Pagina nu a fost gasita');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => { // eslint-disable-line
  res
    .status(err.status || 500)
    .send(err.message);
});

!fs.existsSync('public') && fs.mkdirSync('public');
!fs.existsSync('public/previews') && fs.mkdirSync('public/previews');

export default app;