import { Router } from 'express';

import users from './components/users';
import contact from './components/contact';
import template from './components/templates';

const routes = Router();

routes.get('/', (req, res) => {
  res.send('Hello World');
});

routes.use('/users', users);
routes.use('/contact', contact);
routes.use('/template', template);

export default routes;
